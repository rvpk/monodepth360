import argparse
import os


LEFT_FOLDER_NAME = 'image_02'
RIGHT_FOLDER_NAME = 'image_03'


def read_dataset_paths(path):
    return [os.path.join(path, f) for f in sorted(os.listdir(path)) if not f.startswith('.') and f.endswith('.png')]


def read_dataset(path):
    image_lists = {LEFT_FOLDER_NAME: [], RIGHT_FOLDER_NAME: []}

    for folder in os.listdir(path):
        if os.path.isdir(os.path.join(path, folder)):
            image_lists[LEFT_FOLDER_NAME] += read_dataset_paths(os.path.join(path, folder, LEFT_FOLDER_NAME, 'data'))
            image_lists[RIGHT_FOLDER_NAME] += read_dataset_paths(os.path.join(path, folder, RIGHT_FOLDER_NAME, 'data'))

    if len(image_lists[LEFT_FOLDER_NAME]) != len(image_lists[RIGHT_FOLDER_NAME]):
        print('ERROR: directory {} does not contain the same number of left/right images'.format(path))

    return image_lists[LEFT_FOLDER_NAME], image_lists[RIGHT_FOLDER_NAME]


def write_dataset_into_file(source_file, target_file):
    scene_folders = [line.strip() for line in open(source_file)]

    left_files = []
    right_files = []
    for folder in scene_folders:
        print('process', folder)
        tmp_left_files, tmp_right_files = read_dataset(os.path.join(os.path.dirname(source_file), folder))
        left_files += tmp_left_files
        right_files += tmp_right_files

    with open(target_file, 'w') as text_file:
        for left_file, right_file in zip(left_files, right_files):
            text_file.write(left_file + " " + right_file + "\n")


def data_txt_create(root_folder):
    """Function to create a text file for training from data folder"""
    datasets = [{'destination': 'train.txt', 'source': os.path.join(root_folder, 'training.txt')}]

    for dataset in datasets:
        write_dataset_into_file(dataset['source'], dataset['destination'])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Creates a file with dataset path.')
    parser.add_argument('--data_path', type=str, help='add your dataset folder location')
    args = parser.parse_args()
    data_txt_create(args.data_path)
